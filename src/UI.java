import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by mrlotfi on 12/7/16.
 */
public class UI {
    public static void start() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome. Enter the directory where poems lay bare: ");
        String poems_dir = scanner.nextLine();
        System.out.println("Now enter the directory where the index should be constructed: ");
        String index_dir = scanner.nextLine();
        try {
            Indexer simple_indexer = new Indexer(index_dir, poems_dir);
            simple_indexer.clearIndex();
            int i = simple_indexer.createIndex();
            simple_indexer.close();
            System.out.println(i);
        } catch (IOException e) {
            System.out.println("Fuck that's not a good path. goodbye");
            return;
        }

        System.out.println("ok now entering search...");
        while(true) {
            System.out.println("Search manually(1) or use file query(2)? ");
            String choice = scanner.nextLine();
            if(choice.equals("1")) {
                String query_string = "";
                System.out.println("Enter  poet   ");
                String poet = scanner.nextLine();
                query_string += (poet.length()==0?"":"شاعر:"+poet+" - ");
                System.out.println("Enter book");
                String book = scanner.nextLine();
                query_string += (book.length()==0?"":"کتاب:"+book+" - ");
                System.out.println("Enter selsele");
                String ring = scanner.nextLine();
                query_string += (ring.length()==0?"":"سلسله:"+ring+" - ");
                System.out.println("Enter title");
                String title = scanner.nextLine();
                query_string += (title.length()==0?"":"عنوان:"+title+" - ");
                System.out.println("Enter content");
                String content = scanner.nextLine();
                query_string += (content.length()==0?"":"متن:"+content+" - ");
                query_string = query_string.substring(0,query_string.length()-2);
                query_string += "\n";
                try {
                    Searcher simple_searcher = new Searcher(index_dir);
                    System.out.println("Searching for query: "+query_string+"\n");
                    TopDocs search_result = simple_searcher.search(query_string);
                    for (ScoreDoc score_doc : search_result.scoreDocs) {
                        System.out.print("Got this doc");
                        System.out.println(simple_searcher.getDocument(score_doc).get("filename"));
                        System.out.println("Score is: "+score_doc.score);
                    }
                } catch (IOException e) {
                    System.out.println("Fuck this shit it's all your fault.");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Enter full path to query file: ");
                String query_path = scanner.nextLine();
                try {
                    Searcher simple_searcher = new Searcher(index_dir);
                    File file = new File(query_path);
                    FileInputStream in = new FileInputStream(file);
                    byte[] data = new byte[(int) file.length()];
                    in.read(data);
                    in.close();
                    String query_str = new String(data, "UTF-8");
                    System.out.println("Searching for query: "+FileExtractor.getQueryString(query_str)+"\n");
                    TopDocs search_result = simple_searcher.search(FileExtractor.getQueryString(query_str));
                    for (ScoreDoc score_doc : search_result.scoreDocs) {
                        System.out.println("Got this doc");
                        System.out.println(simple_searcher.getDocument(score_doc).get("filename"));
                        System.out.println("Score is: "+score_doc.score);
                    }
                } catch (IOException e) {
                    System.out.println("Fuck this shit it's all your fault.");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
