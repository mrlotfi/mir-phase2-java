import org.apache.lucene.analysis.fa.PersianAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Paths;

/**
 * Created by mrlotfi on 12/6/16.
 * does the search
 */
public class Searcher {
    public static int TOP_K = 10;
    private IndexSearcher index_searcher;
    private QueryParser query_parser;

    public Searcher(String index_path_str) throws IOException{
        IndexReader index_reader = DirectoryReader.open(
                FSDirectory.open(
                        Paths.get(index_path_str)));
        index_searcher = new IndexSearcher(index_reader);
        query_parser = new QueryParser("متن", new PersianAnalyzer());
    }

    public TopDocs search(String search_str) throws ParseException, IOException{
        Query query = query_parser.parse(search_str);
        return index_searcher.search(query, Searcher.TOP_K);
    }

    public Document getDocument(ScoreDoc scoreDoc) throws IOException{
        return index_searcher.doc(scoreDoc.doc);
    }
}
