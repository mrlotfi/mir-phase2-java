import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.fa.PersianAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * Created by mrlotfi on 12/5/16.
 * index craetion and etc
 */
public class Indexer {
    private IndexWriter index_writer;
    private String poems_path_str;
    public Indexer(String index_path_str, String poems_path_str) throws IOException{
        this.poems_path_str = poems_path_str;
        Path index_path = Paths.get(index_path_str);
        Directory index_directory = FSDirectory.open(index_path);
        IndexWriterConfig index_config = new IndexWriterConfig(new PersianAnalyzer());
        index_writer = new IndexWriter(index_directory, index_config);
    }

    public void clearIndex() throws IOException{
        System.out.println("Clearing index....");
        index_writer.deleteAll();
    }

    public void close() throws IOException {
        index_writer.close();
    }

    public Document getDocument(File file) throws IOException {
        Document document = new Document();
        FileInputStream in = new FileInputStream(file);
        byte[] data = new byte[(int) file.length()];
        in.read(data);
        in.close();
        String doc_str = new String(data, "UTF-8");
        String[] doc_parts = FileExtractor.getParts(doc_str);

        //Define and adding fields
//        System.out.println(doc_parts[0]+ doc_parts[1]+ doc_parts[2]+ doc_parts[3]+ doc_parts[4]);
        Field poet_field = new TextField("شاعر", doc_parts[0], Field.Store.YES);
        Field book_field = new TextField("کتاب", doc_parts[1], Field.Store.YES);
        Field ring_field = new TextField("سلسله", doc_parts[2], Field.Store.YES);
        Field title_field = new TextField("عنوان", doc_parts[3], Field.Store.YES);
        Field content_field = new TextField("متن", doc_parts[4], Field.Store.YES);
        Field filePath_field = new StringField("filename", file.getAbsolutePath(), Field.Store.YES);
//        Field fileNameField = new Field(LuceneConstants.FILE_NAME,
//                file.getName(),
//                Field.Store.YES,Field.Index.NOT_ANALYZED);
//        Field filePathField = new Field(LuceneConstants.FILE_PATH,
//                file.getCanonicalPath(),
//                Field.Store.YES,Field.Index.NOT_ANALYZED);

        document.add(poet_field);
        document.add(book_field);
        document.add(ring_field);
        document.add(title_field);
        document.add(content_field);
        document.add(filePath_field);
        return document;
    }

    public void indexFile(File file) throws IOException{
        System.out.println("Adding this file to index: "+file.getCanonicalPath());
        index_writer.addDocument(getDocument(file));
    }

    public int createIndex() throws IOException {
        File[] files = new File(poems_path_str).listFiles();
        for (File file : files) {
            indexFile(file);
        }
        return index_writer.numDocs();
    }
}
