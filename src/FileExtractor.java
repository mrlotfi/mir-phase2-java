/**
 * Created by mrlotfi on 11/27/2016.
 */
public class FileExtractor {
    public static String[] getParts(String content) {
        String[] parts = new String[5];
        String[] content_parts = content.split("###");
        System.arraycopy(content_parts,1, parts,0,5);
        for(int i=0;i<parts.length;i++) {
            parts[i] = parts[i].substring(parts[i].indexOf("\n")+1);
            if (i != parts.length-1)
              parts[i] = parts[i].substring(0,parts[i].indexOf("\n"));
            else
                parts[i] = parts[i].substring(0, parts[i].lastIndexOf("\n"));
        }

        return parts;
    }

    public static String getQueryString(String content) {
        String[] parts = content.split("###");
        String result = "";
        for (int i=1;i<parts.length;i++) {
            String[] two_parts = parts[i].split(":");
            result += two_parts[0].replace("\n", "").replace(" ", "")+":";
            two_parts[1] = two_parts[1].substring(1,(i == parts.length-1?two_parts[1].length():two_parts[1].length()-1));
            result += two_parts[1] + (i == parts.length-1? "" : " - ");
        }
        return result;
    }
}
